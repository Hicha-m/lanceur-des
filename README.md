# Lanceur-des

modèle de projet de lanceur de dés:

vous devez développer une application permettant de simuler le lancer de deux dés de six faces (le dé classique) avec les fonctionnalités suivantes :

​un bouton permet de déclencher le lancer dé
on affiche la face de chaque dé plutôt que le nombre ( les images sont fournies )
on peut faire des améliorations ou des variantes :

​prendre des dés qui n'ont pas 6 faces
​prendre davantage de dés
​demander à l'utilisateur quel ensemble de dés il veut : on peut utiliser la notation des jeux de rôles. ex: 2d6+1d12 veut dire 2 dés de 6 faces et un dé de 12 faces.
​simuler le roulement du dé (vsible dans l'application d'exemple)
​

la notation tiendra évidement compte de votre capacité a faire faire évoluer le programme.

![alt text](https://gitlab.com/Hicha-m/lanceur-des/-/raw/master/image.png?raw=true)
