from kivy.app import App
from kivy.clock import Clock
from random import uniform, randrange
from re import match


class DiceRollerApp(App):
    # titre de l'aplication
    title = "DiceRoller"
    # liste des images des dés   
    list_dice = ['0.png', '1.png', '2.png', '3.png', '4.png', '5.png', '6.png', '7.png', '8.png', '9.png', '10.png', '11.png', '12.png', '13.png', '14.png', '15.png', '16.png', '17.png', '18.png', '19.png', '20.png']
    # liste du nombre de dés et du nombre de face des dés
    nombres = [1,1] ; faces = [6,6]
    # dictionaire des dés par rapport a leur faces et booléen True : face de 1:10 ? face de 0:9
    formeF =  {x: ('',True) for x in range(4, 21)}
    # Ajout des formes des faces en fonction des faces [4,6,8,10,12,20] et booléen True : face de 1:10 ? face de 0:9
    formeF[4] = 'triangle', True ; formeF[6] = 'carre', True ; formeF[8] = 'triangle', True ; formeF[10] = 'trapeze', False ; formeF[12] = 'pentagone', True ; formeF[20] = 'triangle', True ;
    

    
    def build(self):
        # list des id des images 
        self.images = [self.root.ids.image1,self.root.ids.image2,self.root.ids.image3,self.root.ids.image4]  
        
        
    def start_dice(self, *args):
        #Clock.schedule_interval : se déclanche quand start_dice est lancé et commence une intervale de 0.1sec de la fonction roll
        Clock.schedule_interval(self.roll,uniform(.1, .01))    
        #Clock.schedule_once() : se déclanche a un certain temps pour appelé la fonction stop_dice
        Clock.schedule_once(self.stop_dice,randrange(1,2))

    def stop_dice(self, *args):
        # stope l'intervale
        Clock.unschedule(self.roll)
        # puis lance la fonction total_dice
        self.total_dice()


    def roll(self, dt):
        # variable du nombre d'image apres chaque boucle
        number_image = 0
        # boucle depuis le nombre de dés et de faces
        for nombre,face in zip(self.nombres,self.faces):
            # boucle depuis le nombre de dés
            for x in range(nombre):
                if self.formeF[face][1]:
                    # dés choisie au hasard de 0 à face+1 d'apres la liste de dés
                    randD = self.list_dice[randrange(1, face+1)]
                else:
                    randD = self.list_dice[randrange(0, face)]
                if self.formeF[face][0] != '':
                    self.images[number_image].source = ('./images/dice/'+ self.formeF[face][0]+ '/'+ randD)
                    self.images[number_image].opacity = 1
                    self.images[number_image].size_hint = 1, 1
                else:
                    # remplacer la source de l'image par l'image d'un dés au hasard
                    self.images[number_image].source = ('./images/dice/carre/'+ randD)
                    self.images[number_image].opacity = 1
                    self.images[number_image].size_hint = 1, 1
                # ajout +1 pour l'image suivante
                number_image += 1
 
    def rollprecise(self):
        # variable answer: text du saisie de texte, labelT: le label de texte
        answer = self.root.ids.textinput.text 
        labelT = self.root.ids.label
        # si la reponce fait plus de 3 charactere : 
        if len(answer) >= 3:
            # si elle contient des espaces, alors les supprimer
            if  " " in answer: answer = answer.replace(" ", "")
            # si elle ne contient pas un '+':
            if not "+" in answer:
                # si elle correspond a la regex du format correct pour précisé le nombre dé et nombre de face:
                if bool(match("(^[1-4]d[4-9]$|[1-4]d1[0-9]$|[1-4]d20$)", answer)):
                    # nombre des dés et nombre de faces des dés
                    nb_dice = int(answer[0:1])
                    fc_dice = int(answer[2:]) 
                    # afficher la réponce, son nombre de dé, et ses faces
                    print(answer)
                    print(nb_dice)
                    print(fc_dice)
                    # remplacer le nombre de dé par default par le nouveau et remplacer le nombre de face par default par le nouveau
                    self.nombres = [nb_dice]
                    self.faces = [fc_dice]
                    # boucle pour remettre les images par default apres chaque utilisation
                    for x in range(3, nb_dice-1, -1):
                        self.images[x].source = ''
                        self.images[x].opacity = 0
                        self.images[x].size_hint = 0, 1
                    # afficher le résultat dans le label
                    labelT.text = ("[color=33ff33]Vous avez mis {} dé(s) avec chacun {} faces ![/color]".format(self.nombres[0],self.faces[0]))
                else:
                    labelT.text = ("Le format est incorrect. (max:4d20|min:1d4)")
            else:
                # séparer le str par rapport '+' pour donner la liste de 'answers'
                answers = answer.split("+")
                # la somme des dés
                somme_d = 0
                # énumeration de la boucle
                enum = 0
                # mettre le nombre de dés par default a zero
                self.nombres = []
                # mettre le nombre de face par default a zero
                self.faces = []
                # boucle de chaque reponce:
                for answer in answers:
                    # si elle correspond a la regex du format correct pour précisé le nombre dé et nombre de face:
                    if bool(match("(^[1-4]d[4-9]$|[1-4]d1[0-9]$|[1-4]d20$)", answer)):         
                        # nombre des dés et nombre de faces des dés
                        nb_dice = int(answer[0:1])
                        fc_dice = int(answer[2:])
                        # ajout du nombre de dés à la somme de dé
                        somme_d += nb_dice
                        # si somme_d est plus grand de 0 et plus petit ou égale à 4 
                        if 0 < somme_d <= 4:
                            # afficher la réponce, son nombre de dé, et ses faces
                            print(answer)   
                            print(nb_dice)
                            print(fc_dice)
                            # ajouter le nombre de dé, et ses faces dans la liste de nombres et de faces
                            self.nombres.append(nb_dice)
                            self.faces.append(fc_dice)
                            # ajout +1 à l'énumération
                            enum += 1
                            # si l'énumération de la boucle est égal à la longueur de answers:
                            if enum == len(answers):
                                # boucle pour remettre les images par default apres chaque utilisation
                                for x in range(3, somme_d-1, -1):
                                    self.images[x].source = ''
                                    self.images[x].opacity = 0
                                    self.images[x].size_hint = 0, 1
                                # afficher le résultat dans le label
                                labelT.text = ("[color=33ff33]Vous avez mis {} dé(s) avec {} faces ![/color]".format(self.nombres,self.faces))    
                        else:
                            labelT.text = ("Le nombre de dés est trop élevé le max est de 4 dés. ")
                            # différence du nombre de dés à la somme de dé
                            somme_d -= nb_dice
                            continue       
                    else:
                        labelT.text = ("Le format est incorrect. (max:2d20+2d20|min:1d4+1d4)")
                        continue
        else:
            labelT.text = ("Veuillez réesseyer !")
        
           
    def total_dice(self):
        # la somme des dés
        somme = 0
        # boucle des images
        for image in self.images:
            # si le str est un nombre:
            if image.source[-6:-4].isdigit():
                # ajouter la face du dé à la somme
                somme += int(image.source[-6:-4])
            # sinon si le str est un nombre:
            elif image.source[-5:-4].isdigit():
                # ajouter la face du dé à la somme
                somme += int(image.source[-5:-4])
        # afficher le Total des dés
        self.root.ids.label.text = ("[u]Total des dés : [b]" + str(somme) + "[/b][/u]")



DiceRollerApp().run()